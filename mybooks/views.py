from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.urls import reverse	
import json
import requests

# Create your views here.
def index(request):
	response = {}
	if 'myStars' in request.session.keys():
		response['stars'] = request.session['myStars']
	else:
		request.session['myStars'] = 0
		response['stars'] = request.session['myStars']
	html = 'mybooks.html'
	return render(request, html, response)

def saveBook(request):
	response = {}
	response['saved'] = None
	bookValue = request.GET.get('value', '')
	if 'favorited' in request.session.keys() and 'myStars' in request.session.keys():
		favList = request.session['favorited']
		if bookValue not in favList:
			favList.append(bookValue)
			request.session['favorited'] = favList
			request.session['myStars'] += 1
	else:
		request.session['favorited'] = []
		request.session['favorited'].append(bookValue)
		request.session['myStars'] = 1
	response['saved'] = True
	return JsonResponse(response)

def deleteBook(request):
	response = {}
	response['deleted'] = None
	bookValue = request.GET.get('value', '')
	favList = request.session['favorited']
	favList.remove(bookValue)
	request.session['favorited'] = favList
	request.session['myStars'] -= 1
	response['deleted'] = True
	return JsonResponse(response)

def checkBook(request):
	response = {}
	response['exist'] = None
	bookValue = request.GET.get('value', '')
	if 'favorited' in request.session.keys():
		favList = request.session['favorited']
		if bookValue in favList:
			response['exist'] = True
		else:
			response['exist'] = False
	else:
		response['exist'] = False
	return JsonResponse(response)

def checkLogin(request):
	response = {}
	response['loggedIn'] = None
	if request.user.is_authenticated:
		response['loggedIn'] = True
	else:
		response['loggedIn'] = False
	return JsonResponse(response)

def logout(request):
	request.session.flush()
	return HttpResponseRedirect('/../mybooks')

def getJson(request, category):
	url = "https://www.googleapis.com/books/v1/volumes?q=" + category
	json_data = json.loads(requests.get(url).text)
	return JsonResponse(json_data)