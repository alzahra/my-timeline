from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time
import json

class BookTestcase(TestCase):
    def test_page_exist(self):
        response = Client().get('/aboutme/')
        self.assertEqual(response.status_code, 200)

    def test_page_use_template(self):
        response = Client().get('/mybooks/')
        self.assertTemplateUsed(response, 'mybooks.html')

    def test_page_use_index(self):
        found = resolve('/mybooks/')
        self.assertEqual(found.func, index)

    def test_html_content(self):
        self.client = Client()
        new_response = self.client.get('/mybooks/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('My Favorited Books', html_response)

    def test_favorite_unfavorite_exist(self):
        response = self.client.get('/mybooks/checkBook/?value=abcdef')
        self.assertEqual(json.loads(response.content), {'exist': False})
        response = self.client.get('/mybooks/saveBook/?value=abcdef')
        self.assertEqual(json.loads(response.content), {'saved': True})
        response = self.client.get('/mybooks/saveBook/?value=ghijkl')
        self.assertEqual(json.loads(response.content), {'saved': True})
        response = self.client.get('/mybooks/checkBook/?value=abcdef')
        self.assertEqual(json.loads(response.content), {'exist': True})
        response = self.client.get('/mybooks/deleteBook/?value=abcdef')
        self.assertEqual(json.loads(response.content), {'deleted': True})
        response = self.client.get('/mybooks/checkBook/?value=abcdef')
        self.assertEqual(json.loads(response.content), {'exist': False})

    def test_favorite(self):
        user = User.objects.create(username='alzahra')
        user.set_password('AlyaZahra22')
        user.save()
        login = self.client.login(username='alzahra', password='AlyaZahra22')
        response = self.client.get('/mybooks/saveBook/?value=abcdef')
        self.assertEqual(json.loads(response.content), {'saved': True})
        new_response = self.client.get('/mybooks/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('1', html_response)

    def get_books(self):
        user = User.objects.create(username='alzahra')
        user.set_password('AlyaZahra22')
        user.save()
        login = self.client.login(username='alzahra', password='AlyaZahra22')
        self.client.get('/mybooks/')
        response = self.client.get('/mybooks/jsbkjasb/')
        self.assertIn(json.loads(response.content), {"kind": "books#volumes", "totalItems": 0})

    def login_true(self):
        user = User.objects.create(username='alzahra')
        user.set_password('AlyaZahra22')
        user.save()
        login = self.client.login(username='alzahra', password='AlyaZahra22')
        response = self.client.get('/mybooks/checkLogin/')
        self.assertEqual(json.loads(response.content), {'loggedIn': True})

    def login_false(self):
        response = self.client.get('/mybooks/checkLogin/')
        self.assertEqual(json.loads(response.content), {'loggedIn': False})

    def logout_true(self):
        self.client.post('/mybooks/logout/')
        response = self.client.get('/mybooks/checkLogin/')
        self.assertEqual(json.loads(response.content), {'loggedIn': False})

class BooksFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.browser = webdriver.Chrome('./chromedriver')
        self.browser.implicitly_wait(25)
        super(BooksFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(BooksFunctionalTest, self).tearDown()

    def test_change_theme_to_purple(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mybooks/'))
        selenium.find_element_by_class_name('change').click()
        selenium.find_element_by_class_name('purple-theme').click()
        title = selenium.find_element_by_id('books-box').get_attribute('class')
        self.assertTrue('purple-bg' in title)

    def test_change_theme_to_blue(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mybooks/'))
        selenium.find_element_by_class_name('change').click()
        selenium.find_element_by_class_name('blue-theme').click()
        title = selenium.find_element_by_id('books-box').get_attribute('class')
        self.assertTrue('blue-bg' in title)