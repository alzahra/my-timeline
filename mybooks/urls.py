from django.urls import path
from . import views
from .views import *

app_name = 'mybooks'
urlpatterns = [
    path('', index),
    path('logout/', logout, name='logout'),
    path('saveBook/', saveBook, name='saveBook'),
    path('deleteBook/', deleteBook, name='deleteBook'),
    path('checkBook/', checkBook, name='checkBook'),
    path('checkLogin/', checkLogin, name='checkLogin'),
    path('<str:category>/', getJson, name='getJson'),
]