$(document).ready(function() {
	$('.searchbtn').click(function(){
		checkLogin();
	});

	function showTable() {
		$(function() {
			var category = $('#search-bar').val();
			$.ajax({
				url: "/mybooks/" + category,
				success: function(result) {
                    $('#booklist').empty();
				    $.each(result.items, function(i, item) {
				    	$('<tr>').append(
				        	$('<td>').text(i+1),
				        	$('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
				            $('<td>').text(item.volumeInfo.title),
				            $('<td>').text(item.volumeInfo.authors),
				            $('<td>').text(item.volumeInfo.publisher),
				            $('<td>').text(item.volumeInfo.publishedDate),
				            $('<td>').append('<div class="fav fa fa-star-o" id="' + (i+1) + '" value= "' +
				            	item.id + '"></div>'),
				        ).appendTo('#booklist');
				    	checkBook(item.id, i);
				    });
				}
			});
		});
	}

	function checkLogin() {
		$(function(){
			$.ajax({
				type: 'GET',
				url: "/mybooks/" + 'checkLogin',
				dataType: 'json',
				success: function(result) {
					if (result.loggedIn) {
						showTable();
					}
					else {
						$('#beforeLogin').modal('show');
					}
				}
			});
		});
	}

	function addStar(object) {
		if (object.hasClass("fa-star-o")) {
			saveBook(object.attr('value'));
			object.removeClass("fa-star-o").addClass("fa-star").addClass("yellow-clr");
		} else {
			deleteBook(object.attr('value'));
			object.removeClass("fa-star").removeClass("yellow-clr").addClass("fa-star-o");
		}
	}

	$("tbody").delegate(".fav", "click", function() {
		addStar($(this));
	})

	function saveBook(bookId){
		$(function(){
			var value = bookId;
			$.ajax({
				type: 'GET',
				url: "/mybooks/" + 'saveBook?value=' + value,
				dataType: 'json',
				success: function(result) {
					if (result.saved) {
						var oldStar = $("#number").text()
						var newStar = parseFloat(oldStar) + 1
						$("#number").text(newStar);
					}
				}
			});
		});
	}

	function deleteBook(bookId){
		$(function(){
			var value = bookId;
			$.ajax({
				type: 'GET',
				url: "/mybooks/" + 'deleteBook?value=' + value,
				dataType: 'json',
				success: function(result) {
					if (result.deleted) {
						var oldStar = $("#number").text()
						var newStar = parseFloat(oldStar) - 1
						$("#number").text(newStar);
						boolean = true;
					}
				}
			});
		});
	}

	function checkBook(bookId, i){
		$(function(){
			var value = bookId;
			$.ajax({
				type: 'GET',
				url: "/mybooks/" + 'checkBook?value=' + value,
				dataType: 'json',
				success: function(result) {
					if (result.exist) {
						$('#' + String(i+1)).removeClass("fa-star-o").addClass("fa-star").addClass("yellow-clr");
					}
				}
			});
		});
	}
});