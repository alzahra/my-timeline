from django.shortcuts import render
from django.http import HttpResponseRedirect

def index(request):
    html = 'login.html'
    return render(request, html)