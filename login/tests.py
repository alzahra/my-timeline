from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class LoginTestcase(TestCase):
	def test_login_exist(self):
	    response = Client().get('/login/')
	    self.assertEqual(response.status_code, 200)

	def test_login_use_template(self):
	    response = Client().get('/login/')
	    self.assertTemplateUsed(response, 'login.html')

	def test_login_use_index(self):
	    found = resolve('/login/')
	    self.assertEqual(found.func, index)

	def test_html_login_content(self):
	    new_response = self.client.get('/login/')
	    html_response = new_response.content.decode('utf-8')
	    self.assertIn('LOG-IN HERE', html_response)