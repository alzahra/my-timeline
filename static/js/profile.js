function accordion() {
	$("#activity").click(function(){
        $("#act-ans").slideToggle("slow");
    });
    $("#experience").click(function(){
        $("#exp-ans").slideToggle("slow");
    });
    $("#achieve").click(function(){
        $("#ach-ans").slideToggle("slow");
    });
    $("#skill").click(function(){
        $("#skill-ans").slideToggle("slow");
    });
}

function purpletheme() {
	$(".prof").removeClass("blue-bg").addClass("purple-bg");
	$(".mypanel").removeClass("dblue-bg").addClass("dpurple-bg");
	$(".page-title").removeClass("blue-bg").addClass("purple-bg");
	$(".save").removeClass("dblue-bg").addClass("dpurple-bg");
	$(".progress-bar").removeClass("blue-bg").addClass("purple-bg");
	$(".fa-circle").removeClass("blue-clr").addClass("purple-clr");
	$(".fa-circle-o-notch").removeClass("blue-clr").addClass("purple-clr");
	$(".top-box").removeClass("blue-bg").addClass("purple-bg");
}

function bluetheme() {
	$(".prof").removeClass("purple-bg").addClass("blue-bg");
	$(".mypanel").removeClass("dpurple-bg").addClass("dblue-bg");
	$(".page-title").removeClass("purple-bg").addClass("blue-bg");
	$(".save").removeClass("dpurple-bg").addClass("dblue-bg");
	$(".progress-bar").removeClass("purple-bg").addClass("blue-bg");
	$(".fa-circle").removeClass("purple-clr").addClass("blue-clr");
	$(".fa-circle-o-notch").removeClass("purple-clr").addClass("blue-clr");
	$(".top-box").removeClass("purple-bg").addClass("blue-bg");
}

$(document).ready(function() {
	$("#loader").css('display', 'none');
	$(".content").css('display', 'block');
	
	if (!localStorage) {
		localStorage.theme = "blue";
	}

	switch(localStorage.theme) {
		case "blue":
			bluetheme();
			break;
		case "purple":
			purpletheme()
			break;
	}

	$(".blue-theme").click(function(){
		bluetheme();
		localStorage.theme = "blue";
	});

	$(".purple-theme").click(function(){
		purpletheme();
		localStorage.theme = "purple";
	});

    accordion();
});