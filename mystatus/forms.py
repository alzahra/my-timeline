from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    status_line = forms.CharField(max_length=300, widget=forms.Textarea(attrs={'class' : 'form-status', 'placeholder' : 'Write your status here'}))

    class Meta:
        model = Status
        fields = ('status_line',)
        fields_required = ('status_line',)