from django.urls import path
from . import views
from .views import index, savestatus

app_name = 'mystatus'
urlpatterns = [
    path('', index),
    path('savestatus/', views.savestatus, name='savestatus'),
]