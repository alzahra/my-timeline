from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status

response = {}

def index(request):
    status = Status.objects.all()
    response['status'] = status
    response['form'] = StatusForm
    html = 'status.html'
    return render(request, html, response)

def savestatus(request):
    form = StatusForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['status_line'] = request.POST['status_line']
        status = Status(status_line=response['status_line'])
        status.save()
        return HttpResponseRedirect('/mystatus/')
    return render(request, 'status.html', {'form':form})