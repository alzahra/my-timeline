from django.test import TestCase, Client
from django.urls import resolve
from .models import Status
from .views import index, savestatus
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time

class StatusTestcase(TestCase):
    def test_status_exist(self):
        response = Client().get('/mystatus/')
        self.assertEqual(response.status_code, 200)

    def test_status_use_template(self):
        response = Client().get('/mystatus/')
        self.assertTemplateUsed(response, 'status.html')

    def test_status_use_index(self):
        found = resolve('/mystatus/')
        self.assertEqual(found.func, index)

    def test_model_can_create_status(self):
            new_status = Status.objects.create(status_line='Stress overload')
            available_status = Status.objects.all().count()
            self.assertEqual(available_status, 1)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/mystatus/savestatus/', data={'status_line' : 'Stress overload'})
        available_status = Status.objects.all().count()
        self.assertEqual(available_status, 1)
        self.assertEqual(response.status_code, 302)
    
    def test_cannot_save_a_POST_request(self):
        response = self.client.post('/mystatus/savestatus/', data={'status_line' : 'Stress overload' * 300 })
        available_status = Status.objects.all().count()
        self.assertEqual(available_status, 0)
        self.assertEqual(response.status_code, 200)

    def test_html_content(self):
        new_response = self.client.get('/mystatus/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('Hello, apa kabar?', html_response)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-status"', form.as_p())
        self.assertIn('placeholder="Write your status here"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status_line': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status_line'],
            ["This field is required."]
        )

class StatusFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.browser = webdriver.Chrome('./chromedriver')
        self.browser.implicitly_wait(25)
        super(StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mystatus/'))
        status_line = selenium.find_element_by_id('id_status_line')
        submit = selenium.find_element_by_id('submit')
        status_line.send_keys('Coba Coba')
        submit.click()
        time.sleep(5)
        self.assertIn('Coba Coba', selenium.page_source)

    def test_page_title(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mystatus/'))
        page_title = selenium.find_element_by_id('hello-title').text
        self.assertEqual('Hello, apa kabar?', page_title)

    def test_page_title_css(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mystatus/'))
        page_title = selenium.find_element_by_class_name('page-title').value_of_css_property('font-family')
        self.assertEqual('Muli', page_title)

    def test_navbar_css(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mystatus/'))
        navbar = selenium.find_element_by_class_name('navbar-expand-sm').value_of_css_property('background-color')
        self.assertEqual('rgba(0, 0, 0, 1)', navbar)

    def test_change_theme_to_purple(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mystatus/'))
        selenium.find_element_by_class_name('change').click()
        selenium.find_element_by_class_name('purple-theme').click()
        title = selenium.find_element_by_id('hello-title').get_attribute('class')
        self.assertTrue('purple-bg' in title)
        button = selenium.find_element_by_id('submit').get_attribute('class')
        self.assertTrue('purple-bg' in button)

    def test_change_theme_to_blue(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/mystatus/'))
        selenium.find_element_by_class_name('change').click()
        selenium.find_element_by_class_name('blue-theme').click()
        title = selenium.find_element_by_id('hello-title').get_attribute('class')
        self.assertTrue('blue-bg' in title)
        button = selenium.find_element_by_id('submit').get_attribute('class')
        self.assertTrue('blue-bg' in button)