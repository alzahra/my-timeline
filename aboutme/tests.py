from django.test import TestCase, Client
from django.urls import resolve
from .views import index, subscribePage
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time

class ProfileTestcase(TestCase):
    def test_profile_exist(self):
        response = Client().get('/aboutme/')
        self.assertEqual(response.status_code, 200)

    def test_profile_use_template(self):
        response = Client().get('/aboutme/')
        self.assertTemplateUsed(response, 'aboutme.html')

    def test_profile_use_index(self):
        found = resolve('/aboutme/')
        self.assertEqual(found.func, index)

    def test_html_content(self):
        new_response = self.client.get('/aboutme/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('ALYA ZAHRA', html_response)

    def test_sub_exist(self):
        response = Client().get('/aboutme/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_sub_use_template(self):
        response = Client().get('/aboutme/subscribe/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_sub_use_func(self):
        found = resolve('/aboutme/subscribe/')
        self.assertEqual(found.func, subscribePage)

    def test_html_content_sub(self):
        new_response = self.client.get('/aboutme/subscribe/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('Subscribe to this site now!', html_response)

    def test_fail_sub(self):
        response = self.client.post('/aboutme/subscribe/submitForm', data={'Email' : 'alya', 'Name' : 'a', 
            'Password':'12345678'})
        self.assertEqual(response.status_code, 403)

    def test_getPass(self):
        response = self.client.post('/aboutme/subscribe/submitForm', data={'Email' : 'alya@yahoo.com', 'Name' : 'alya', 
            'Password':'12345678'})
        data = self.client.get('/aboutme/subscribe/getPass')
        html_response = data.content.decode('utf-8')
        self.assertIn('"Email": "alya@yahoo.com"', html_response)

class ProfileFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.browser = webdriver.Chrome('./chromedriver')
        self.browser.implicitly_wait(25)
        super(ProfileFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(ProfileFunctionalTest, self).tearDown()

    def test_change_theme_to_purple(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/aboutme/'))
        selenium.find_element_by_class_name('change').click()
        selenium.find_element_by_class_name('purple-theme').click()
        profile = selenium.find_element_by_id('prof').get_attribute('class')
        self.assertTrue('purple-bg' in profile)
        panel = selenium.find_element_by_name('panel').get_attribute('class')
        self.assertTrue('dpurple-bg' in panel)

    def test_change_theme_to_blue(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/aboutme/'))
        selenium.find_element_by_class_name('change').click()
        selenium.find_element_by_class_name('blue-theme').click()
        profile = selenium.find_element_by_id('prof').get_attribute('class')
        self.assertTrue('blue-bg' in profile)
        panel = selenium.find_element_by_name('panel').get_attribute('class')
        self.assertTrue('dblue-bg' in panel)

    def test_accordion(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/aboutme/'))
        answer_display = selenium.find_element_by_class_name('answer').value_of_css_property('display')
        self.assertEqual('none', answer_display)
        selenium.find_element_by_class_name('mypanel').click()
        answer_display = selenium.find_element_by_class_name('answer').value_of_css_property('display')
        self.assertEqual('block', answer_display)

    def test_invalid_form(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/aboutme/subscribe'))
        email = selenium.find_element_by_id('id_Email')
        name = selenium.find_element_by_id('id_Name')
        password = selenium.find_element_by_id('id_Password')
        email.send_keys('alya')
        name.send_keys('')
        password.send_keys('1234')
        email.click()

        emailError = selenium.find_element_by_id('Email_error').text
        nameError = selenium.find_element_by_id('Name_error').text
        passError = selenium.find_element_by_id('Password_error').text
        self.assertEqual('Enter a valid email address.', emailError)
        self.assertEqual('This field cannot be empty', nameError)
        self.assertEqual('Password must be a minimum of 8 characters', passError)
        button = selenium.find_element_by_id('subbutton')
        self.assertEqual('true', button.get_attribute('disabled'))

    def test_valid_form(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/aboutme/subscribe'))
        email = selenium.find_element_by_id('id_Email')
        name = selenium.find_element_by_id('id_Name')
        password = selenium.find_element_by_id('id_Password')
        email.send_keys('alya@yahoo.com')
        name.send_keys('alya')
        password.send_keys('12345678')
        email.click()
        button = selenium.find_element_by_id('subbutton')
        self.assertEqual(None, button.get_attribute('disabled'))
        button.click()
        time.sleep(5)
        success = selenium.find_element_by_id('success').value_of_css_property('display')
        self.assertEqual('block', success)

    def test_email_is_used(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/aboutme/subscribe'))
        email = selenium.find_element_by_id('id_Email')
        name = selenium.find_element_by_id('id_Name')
        password = selenium.find_element_by_id('id_Password')
        email.send_keys('alya@yahoo.com')
        name.send_keys('alya')
        password.send_keys('12345678')
        email.click()
        button = selenium.find_element_by_id('subbutton')
        button.click()
        time.sleep(5)

        email = selenium.find_element_by_id('id_Email')
        email.send_keys('alya@yahoo.com')
        password = selenium.find_element_by_id('id_Password')
        password.click()
        time.sleep(5)
        emailError = selenium.find_element_by_id('Email_error').text
        self.assertEqual('This email has been used before, please use another email', emailError)

    def test_unsubscribe_fail_then_sucess(self):
        selenium = self.browser
        selenium.get('%s%s' % (self.live_server_url, '/aboutme/subscribe'))
        email = selenium.find_element_by_id('id_Email')
        name = selenium.find_element_by_id('id_Name')
        password = selenium.find_element_by_id('id_Password')
        email.send_keys('alya@yahoo.com')
        name.send_keys('alya')
        password.send_keys('12345678')
        email.click()
        button = selenium.find_element_by_id('subbutton')
        button.click()
        time.sleep(5)

        unsub = selenium.find_element_by_id("1")
        unsub.click()
        time.sleep(2)
        secondPass = selenium.find_element_by_id('id_confirmpass')
        secondPass.send_keys('aaaaaaaa')
        confirm = selenium.find_element_by_id('confirmunsub')
        confirm.click()
        time.sleep(2)
        confirmError = selenium.find_element_by_id('confirmpass_error').text
        self.assertEqual('Your password does not match', confirmError)

        secondPass = selenium.find_element_by_id('id_confirmpass')
        secondPass.send_keys('12345678')
        confirm.click()
        time.sleep(2)
        success = selenium.find_element_by_id('afterUnsub').value_of_css_property('display')
        self.assertEqual('block', success)