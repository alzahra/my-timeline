from django.shortcuts import render
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from .forms import SubscribeForm
from .models import Subscribe
from django.core import serializers
import json


def index(request):
    html = 'aboutme.html'
    return render(request, html)

def subscribePage(request):
    response = {}
    response['form'] = SubscribeForm
    html = 'subscribe.html'
    return render(request, html, response)

def check_email(request):
    email = request.GET.get('q', '')
    response = {'valid' : None, 'exist' : None, 'messages' : []}
    try:
        validate_email(email)
    except ValidationError as error:
        response['valid'] = False
        response['messages'] = error.messages
    else:
        response['valid'] = True
        if (Subscribe.objects.filter(Email=email)):
            response['exist'] = True
            response['messages'] = ['This email has been used before, please use another email']
        else:
            response['exist'] = False
    return JsonResponse(response)

def submitForm(request):
    form = SubscribeForm(request.POST or None)
    response = {'success' : None}
    if form.is_valid():
        form.save()
        response['success'] = True
        return JsonResponse(response)
    else:
        response = JsonResponse({'success': False})
        response.status_code = 403
        return response

def getSubscriber(request):
    subscriber = list(Subscribe.objects.values('Name', 'Email').order_by('Name'))
    return JsonResponse({'subscribers': subscriber})

def getPass(request):
    subscriber = list(Subscribe.objects.values('Name', 'Email', 'Password').order_by('Name'))
    return JsonResponse({'subscribers': subscriber})

def checkPass(request):
    email = request.GET.get('email', '')
    response = {'match' : None}
    password = request.GET.get('pass', '')
    userPass = Subscribe.objects.filter(Email=email).values_list('Password', flat=True)
    if (password == list(userPass)[0]):
        response['match'] = True
    else:
        response['match'] = False
    return JsonResponse(response)

def deleteSubscriber(request):
    user = request.GET.get('email', '')
    response = {}
    Subscribe.objects.filter(Email=user).delete()
    response['deleted'] = True
    return JsonResponse(response)