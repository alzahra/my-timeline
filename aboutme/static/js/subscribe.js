emailValid = false;
nameValid = false;
passwordValid = false;

$(document).ready(function(){
	showTable();
	checkUnsubPassword();
	clearModal();
	clearError();

	function changeModalButtonValue(newval) {
		$('#confirmunsub').val(newval);
	}

	$("#sublist").delegate(".unsub", "click", function() {
		clearModal();
		changeModalButtonValue($(this).val());
		$('#unsubmodal').modal('show');
	});

	function clearModal() {
		$('#id_confirmpass').val('');
		$('#confirmunsub').val('');
	}

	function showTable(){
		$(function() {
			$.ajax({
				type: 'GET',
				url: 'getSubscriber',
				dataType: 'json',
				success: function(result){
					$('#sublist').empty();
					$.each(result.subscribers, function(i, sub) {
				        $('<tr>').append(
				        	$('<td>').text(i+1),
				            $('<td>').text(sub.Name),
				            $('<td>').text(sub.Email),
				            $('<td>').append('<button class="x unsub btn btn-danger white-text" id="' + (i+1) + 
				            	'" value="'+ sub.Email + '">Unsubscribe</button>'),
				        ).appendTo('#sublist');
				    });
				}
			});
		});
	}

	function checkUnsubPassword(){
		$('#confirmunsub').click(function(){
			$(function(){
				pass = $('#id_confirmpass').val()
				email = $('#confirmunsub').val()
				$.ajax({
					type: 'GET',
					url: 'checkPass?email=' + email +'&pass=' + pass,
					dataType: 'json',
					success: function(result) {
						if (result.match) {
							$('#unsubmodal').modal('hide');
							deleteSubscriber(email);
						}
						else {
							$('#id_confirmpass').val('');
							createDiv('confirmpass');
							appendMessage('Your password does not match', 'confirmpass');
						}
					}
				});
			});
		});
	}

	function clearError() {
		$('#id_confirmpass').click(function(){
			$('#confirmpass_error').empty();
		});
	}

	function deleteSubscriber(email){
		$(function(){
			$.ajax({
				type: 'GET',
				url: 'deleteSubscriber?email=' + email,
				dataType: 'json',
				success: function(result) {
					$('#afterUnsub').modal('show');
					showTable();
				}
			});
		})
	}

	$('#subscribeForm').on('focusout', '#id_Email', function(){
	    checkEmail($('#id_Email').val());
	});

	$('#subscribeForm').on('focusout', '#id_Name', function(){
	    checkName($('#id_Name').val());
	});

	$('#subscribeForm').on('focusout', '#id_Password', function(){
	    checkPassword($('#id_Password').val());
	});

	$('#id_Email').click(function(){
		$('#success').hide();
		$('#fail').hide();
	});

	$('#id_Name').click(function(){
		$('#success').hide();
		$('#fail').hide();
	});

	$('#id_Password').click(function(){
		$('#success').hide();
		$('#fail').hide();
	});

	function validateForm() {
		checkEmail($('#id_Email').val());
		checkName($('#id_Name').val());
		checkPassword($('#id_Password').val());
	}

	function changeSubmit() {
		if (emailValid && nameValid && passwordValid) {
	        $('#subbutton').prop('disabled', false);
	    } else {
	        $('#subbutton').prop('disabled', true);
	    }
	}
	
	$('#subscribeForm').submit(function(event){
		var formData = $(this).serializeArray();
		$.ajax({
			type: 'POST',
			url: 'submitForm',
			data: formData,
			dataType: 'json',
			success: function(result) {
				if (result.success) {
					$('#fail').hide();
					$('#success').show();
					showTable();
				} else {
					$('#success').hide();
					$('#fail').show();
				}
				clearAll();
				$('#subbutton').prop('disabled', true);
			}
		});
		event.preventDefault();	
	});

	function checkEmail(email){
		$(function() {
			$.ajax({
				type: 'GET',
				url: 'check_email?q=' + email,
				dataType: 'json',
				success: function(result) {
					if (result.valid && !result.exist) {
						$('#Email_error').empty();
						emailValid = true;
					}
					else {
						emailValid = false;
						createDiv('Email');
						$.each(result.messages, function(i, message){
							appendMessage(message, 'Email');
						});
					}
					changeSubmit();
				}
			});
		});	
	}

	function checkName(name){
		if (name.length > 0) {
			$('#Name_error').empty();
			nameValid = true;
		} else {
			createDiv('Name');
			appendMessage('This field cannot be empty', 'Name');
			nameValid = false;
		}
		changeSubmit();
	}

	function checkPassword(password){
		if (password.length >= 8) {
			$('#Password_error').empty();
			passwordValid = true;
		} else {
			createDiv('Password');
			appendMessage('Password must be a minimum of 8 characters', 'Password');
			passwordValid = false;
		}
		changeSubmit();
	}

	function createDiv(item){
		if ($('#' + item + '_error').length) {
			$('#' + item + '_error').empty();
		} else {
			$('#id_' + item).after($('<div id="'+ item + '_error"></div>'));
		}
	}

	function appendMessage(msg, element){
		$('<p class="x text-danger text-center">').text(msg).appendTo('#' + element + '_error');
	}

	function clearField(field) {
		$('#id_' + field).val('');
	}

	function clearAll() {
		clearField('Email');
		clearField('Name');
		clearField('Password');
	}
});