from django.db import models
from django.core.validators import MinLengthValidator

class Subscribe(models.Model):
	Email = models.EmailField(max_length=100, unique=True)
	Name = models.CharField(max_length=50)
	Password = models.CharField(max_length=30, validators=[MinLengthValidator(8)])