from django import forms
from .models import Subscribe

class SubscribeForm(forms.ModelForm):
    Email = forms.EmailField(max_length=50, widget=forms.EmailInput(
    	attrs={'class' : 'form-control', 'placeholder' : 'Please fill out your email'}))
    Name = forms.CharField(max_length=20, widget=forms.TextInput(
    	attrs={'class' : 'form-control', 'placeholder' : 'Please fill out your name'}))
    Password = forms.CharField(max_length=30, widget=forms.PasswordInput(
    	attrs={'class' : 'form-control', 'placeholder' : 'Please fill out your password'}))

    class Meta:
        model = Subscribe
        fields = ('Email', 'Name', 'Password')
        fields_required = ('Email', 'Name', 'Password')