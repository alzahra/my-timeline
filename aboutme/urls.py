from django.urls import path
from . import views
from .views import *

app_name = 'aboutme'
urlpatterns = [
    path('', index, name='index'),
    path('subscribe/', subscribePage, name='subscribePage'),
    path('subscribe/check_email', check_email, name='check_email'),
    path('subscribe/submitForm', submitForm, name='submitForm'),
    path('subscribe/getSubscriber', getSubscriber, name='getSubscriber'),
    path('subscribe/getPass', getPass, name='getPass'),
    path('subscribe/checkPass', checkPass, name='checkPass'),
    path('subscribe/deleteSubscriber', deleteSubscriber, name='deleteSubscriber'),
]